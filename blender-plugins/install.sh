#!/bin/bash

SCRIPTPATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

[ -d $HOME/.config/blender/2.79/scripts/addons/ ] || mkdir -p $HOME/.config/blender/2.79/scripts/addons/

rm -rf $HOME/.config/blender/2.79/scripts/addons/coa_tools || true
cp -r $SCRIPTPATH/coa_tools $HOME/.config/blender/2.79/scripts/addons/
