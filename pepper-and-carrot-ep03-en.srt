1
00:00:00,320 --> 00:00:02,060
Morevna Project...

2
00:00:02,080 --> 00:00:05,230
...in association with Singpolyma presents.

3
00:00:05,980 --> 00:00:07,680
Pepper & Carrot.

4
00:00:07,710 --> 00:00:11,780
Episode 3: The Secret Ingredients.

5
00:00:11,780 --> 00:00:19,710
Voices in the background:

6
00:00:19,710 --> 00:00:21,330
Good morning Sir.

7
00:00:21,330 --> 00:00:23,780
I would like eight pumpkinstars, please.

8
00:00:23,780 --> 00:00:26,780
Here you are, that's 60Ko.

9
00:00:26,780 --> 00:00:28,480
Oh dear...

10
00:00:28,480 --> 00:00:30,920
Sorry, I'll have to take only four...

11
00:00:30,920 --> 00:00:31,980
Grrr...

12
00:00:31,980 --> 00:00:33,360
Greetings, good Sir.

13
00:00:33,380 --> 00:00:35,980
Please prepare two dozen of everything for me.

14
00:00:35,980 --> 00:00:37,840
Premium quality, as usual.

15
00:00:37,840 --> 00:00:40,480
It's always a pleasure to serve you, Miss Saffron.

16
00:00:40,480 --> 00:00:42,440
Hey look, it's Pepper.

17
00:00:42,440 --> 00:00:44,520
So, how is it going in the countryside?

18
00:00:44,520 --> 00:00:47,230
Let me guess business is booming?

19
00:00:47,230 --> 00:00:49,480
I guess you're preparing the ingredients...

20
00:00:49,480 --> 00:00:51,980
...for tomorrow's potion challenge?

21
00:00:51,980 --> 00:00:54,220
A potion challenge?

22
00:00:54,250 --> 00:00:55,180
Tomorrow?!

23
00:00:56,980 --> 00:01:00,560
The Potion Challenge. 50 000Ko grand prize.

24
00:01:00,560 --> 00:01:03,560
Lucky me, I still have a full day to prepare!

25
00:01:03,560 --> 00:01:05,980
Let's win this challenge!

26
00:01:11,460 --> 00:01:12,730
Oh, I know!

27
00:01:12,730 --> 00:01:14,500
That's exactly what I need!

28
00:01:14,500 --> 00:01:18,420
Carrot, get ready, we'll hunt the ingredients together.

29
00:01:18,980 --> 00:01:23,640
First, we need a few pearls of mist from those black clouds...

30
00:01:27,180 --> 00:01:31,140
...and some red berries from the haunted jungle...

31
00:01:34,140 --> 00:01:38,980
...plus egg shells from the Phoenix valley of the volcanos...

32
00:01:41,040 --> 00:01:45,280
...and finally a drop of milk from a young DragonCow.

33
00:01:49,180 --> 00:01:52,480
That's it Carrot! I think I have everything I need.

34
00:02:00,920 --> 00:02:03,730
It looks... Ahhh... Perfect...

35
00:02:08,020 --> 00:02:11,620
Mmm! The best coffee ever!

36
00:02:11,980 --> 00:02:15,380
It's everything I'll need to work all night long...

37
00:02:15,380 --> 00:02:20,440
...and make the best potion for tomorrow's challenge.

