<palette id='1'>
  
  <version>
    71 0 
  </version>
  <styles>
    <style>
      color_0 3 255 255 255 0 
    </style>
    <style>
      contur 4001 "classic/brush.myb"15 96 115 255 0 
    </style>
    <style>
      color_2 4001 "deevad/rigger_brush.myb"0 0 0 255 0 
    </style>
    <style>
      color_3 3 188 66 55 255 
    </style>
    <style>
      color_4 4001 "classic/blur.myb"64 57 127 255 0 
    </style>
    <style>
      color_5 4001 "deevad/brush.myb"64 57 127 255 0 
    </style>
    <style>
      koja 3 225 169 132 255 
    </style>
    <style>
      rubashka 3 239 214 177 255 
    </style>
    <style>
      "poloska 1"4001 "deevad/liner.myb"195 51 59 255 0 
    </style>
    <style>
      "poloska 2"3 121 23 26 255 
    </style>
    <style>
      jaket 3 128 42 53 255 
    </style>
    <style>
      ubka 3 177 86 72 255 
    </style>
    <style>
      volosi 4001 "deevad/liner.myb"119 44 37 255 0 
    </style>
    <style>
      shlyapa 3 189 82 57 255 
    </style>
    <style>
      tarelka 4001 "deevad/liner.myb"255 235 202 255 0 
    </style>
    <style>
      "remen 1"3 80 32 31 255 
    </style>
    <style>
      "remen 2"4001 "deevad/rigger_brush.myb"239 187 65 255 0 
    </style>
    <style>
      color_17 4001 "deevad/detail_brush_large.myb"216 114 25 255 0 
    </style>
    <style>
      color_18 3 80 98 117 255 
    </style>
    <style>
      color_19 3 237 149 67 255 
    </style>
    <style>
      color_20 3 198 88 20 255 
    </style>
    <style>
      color_21 3 71 36 31 255 
    </style>
    <style>
      color_22 4001 "deevad/rigger_brush.myb"137 205 247 255 0 
    </style>
    <style>
      color_23 4001 "deevad/soft-dip-pen.myb"255 202 77 255 0 
    </style>
    <style>
      color_24 3 107 69 124 255 
    </style>
    </styles>
  <stylepages>
    <page>
      <name>
        "цвета"
      </name>
      <indices>
        0 1 2 6 7 8 9 10 11 12 13 14 15 16 3 4 5 17 18 19 20 21 22 23 24 
      </indices>
      </page>
    </stylepages>
  <shortcuts>
    12 0 1 2 6 7 8 9 10 11 
  </shortcuts>
  
</palette>
