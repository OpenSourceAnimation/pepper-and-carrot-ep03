#!/bin/sh

# This script is used to create dub-pack - it extracts audio tracks
# from blender file and encodes them to FLAC.
# Read more about dub pack here - 
# https://morevnaproject.org/2016/08/22/call-for-fandub/

PREFIX=`dirname $0`
BLENDFILE="${PREFIX}/../project-en.blend"

cd ${PREFIX}/..

set -e

# Voice
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 14
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 15
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 16
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 17

# Music
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 12 11 10

# FX
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 3
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 4
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 5
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 6
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 7
bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 8

#BLENDFILE="${PREFIX}/../ep03-ru.blend"

#bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 22
#bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 23
#bash ${PREFIX}/ExportAudioChannels.sh "${BLENDFILE}" 24


